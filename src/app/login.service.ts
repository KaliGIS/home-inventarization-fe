import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, mapTo, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';

const jwtHelper = new JwtHelperService();

interface User {
  username: string,
  password: string
}

interface Token {
  jwt: string
}

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private readonly JWT_TOKEN = 'JWT_TOKEN';
  private loggedUser: string;

  constructor(
    private http: HttpClient
  ) { }

  login(userData: User) {
    console.log('userData is: ' + userData.username + ', ' + userData.password);
    return this.http.post('/api/login', {
      username: userData.username,
      password: userData.password
    }).pipe(
      tap(token => this.doLoginUser(userData.username, token)),
      mapTo(true),
      catchError(error => {
        console.log('Error occured ' + JSON.stringify(error));
        return of(false);
      })
    );
  }

  logout() {
    this.doLogoutUser();
  }
  
  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  // isLoggedIn() {
  //   if (this.loggedUser) {
  //     return true;
  //   }

  //   return false;
  // }

  isAuthenticated(): boolean {
    const token = localStorage.getItem(this.JWT_TOKEN);
    return !jwtHelper.isTokenExpired(token);
  }

  private doLoginUser(username: string, token) {
    this.loggedUser = username;
    this.storeToken(token);
  }

  private doLogoutUser() {
    this.loggedUser = null;
    this.removeToken();
  }

  private storeToken(token: Token) {
    localStorage.setItem(this.JWT_TOKEN, token.jwt);
  }

  private removeToken() {
    console.log('Removing JWT token');
    localStorage.removeItem(this.JWT_TOKEN);
  }
}
