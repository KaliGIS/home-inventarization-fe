import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { catchError, finalize } from 'rxjs/operators';
import { LoginService } from './login.service';
import { PlaceService } from './place.service';

interface PlaceElement {
    id: number,
    name: string;
    location: string;
    imageUrl: string;
}

export class PlacesDataSource implements DataSource<PlaceElement> {
    private placesSubject = new BehaviorSubject<PlaceElement[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
  
    public loading$ = this.loadingSubject.asObservable();
  
    constructor(
      private placeService: PlaceService,
      private loginService: LoginService,
      private router: Router
    ) {
  
    }
    connect(collectionViewer: CollectionViewer): Observable<PlaceElement[]> {
        return this.placesSubject.asObservable();
    }
    disconnect(collectionViewer: CollectionViewer): void {
        this.placesSubject.complete();
        this.loadingSubject.complete();
    }

    loadPlaces() {
        this.loadingSubject.next(true);
        
        if (this.loginService.isAuthenticated()) {
          this.placeService.getPlaceData().pipe(
            catchError(() => of([])),
            finalize(() => this.loadingSubject.next(false))
          )
          .subscribe(places => {
            this.placesSubject.next(places);
          });
        } else {
          this.router.navigate(['/login']);
        }
      }
}
