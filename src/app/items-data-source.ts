import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { ItemService } from './item.service';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

interface ItemElement {
    id: number,
    name: string;
    note: string;
    place: string;
}

export class ItemsDataSource implements DataSource<ItemElement> {
    private itemsSubject = new BehaviorSubject<ItemElement[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
  
    public loading$ = this.loadingSubject.asObservable();
  
    constructor(
      private itemService: ItemService,
      private loginService: LoginService,
      private router: Router
    ) {
  
    }
  
    connect(collectionViewer: CollectionViewer): Observable<ItemElement[]> {
      return this.itemsSubject.asObservable();
    }
  
    disconnect(collectionViewer: CollectionViewer): void {
      this.itemsSubject.complete();
      this.loadingSubject.complete();
    }
  
    loadItems(pageNumber: number, pageSize: number) {
      this.loadingSubject.next(true);
      
      if (this.loginService.isAuthenticated()) {
        this.itemService.getItemData(pageNumber, pageSize).pipe(
          catchError(() => of([])),
          finalize(() => this.loadingSubject.next(false))
        )
        .subscribe(items => {
          this.itemsSubject.next(items);
        });
      } else {
        this.router.navigate(['/login']);
      }
    }
  }
  
  