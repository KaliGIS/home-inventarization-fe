import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { ItemService } from '../item.service';

interface Place {
  id: number,
  location: string,
  name: string
}

interface NewItemData {
  name: string,
  note: string,
  places: Array<Place>
};

@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.css']
})
export class NewItemComponent{
  selectedPlace: Place

  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  noteFormControl = new FormControl('');
  placeFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(
    public dialogRef: MatDialogRef<NewItemComponent>,
    @Inject(MAT_DIALOG_DATA) public data: NewItemData,
    private itemService: ItemService
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(data) {
    console.log('Form submitted');
    if (this.nameFormControl.valid && this.placeFormControl.valid) {
      console.log('Name and place form controls are valid');
      this.dialogRef.close();
      
      let itemToSend = {
        name: data.name,
        note: data.note
      }

      this.itemService.addNewItem(this.selectedPlace.id, itemToSend).subscribe(responseData => {
        console.log(responseData);
      });
    } else {
      console.log('Name and place form controls are not valid');
    }
  }
}
