import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field'; 
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';

const material = [
  MatInputModule,
  MatTableModule,
  MatButtonModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatDialogModule,
  MatIconModule,
  MatSelectModule
];

@NgModule({
  imports: [],
  exports: [material]
})
export class MaterialModule { }
