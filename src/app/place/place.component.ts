import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DeletePromptComponent } from '../delete-prompt/delete-prompt.component';
import { LoginService } from '../login.service';
import { NewPlaceComponent } from '../new-place/new-place.component';
import { PlaceService } from '../place.service';
import { PlacesDataSource } from '../places-data-source';

@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.css', '../app.component.css']
})
export class PlaceComponent implements OnInit {
  placeColumns: string[] = ['id', 'name', 'location', 'image', 'action'];
  placeData: PlacesDataSource;

  constructor(
    private placeService: PlaceService,
    private loginService: LoginService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.placeData = new PlacesDataSource(this.placeService, this.loginService, this.router);
    this.placeData.loadPlaces();
  }

  openNewPlaceDialog(): void {
    let emptyPlace = {
      name: '',
      location: '',
      // image: todo
    }  
    
    const dialogRef = this.dialog.open(NewPlaceComponent, {
      width: '250px',
      data: emptyPlace
    });

    dialogRef.afterClosed().subscribe(newPlace => {
      console.log('The dialog was closed');
      console.log(newPlace);
      this.placeData.loadPlaces();
    });
    
  }

  openShouldDeleteDialog(row): void {
    const dialogRef = this.dialog.open(DeletePromptComponent, {
      data: row
    });

    dialogRef.afterClosed().subscribe(shouldDelete => {
      console.log('Should delete: ' + shouldDelete);
      console.log('Type of shouldDelete: ' + typeof shouldDelete);
      
      if (shouldDelete) {
        this.placeService.deleteExistingPlace(row).subscribe(
          res => console.log('HTTP response', res),
          err => console.error('HTTP Error', err),
          () => {
            console.log('HTTP request completed.');
            this.placeData.loadPlaces();
          }
        );
      }
    });
  }
}
