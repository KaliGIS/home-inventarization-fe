import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

interface ItemRecord {
  id: number,
  name: string,
  note: string,
  place: object
}

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  
  constructor(
    private http: HttpClient
  ) { }

  getItemData(pageNumber = 0, pageSize = 10) {
    return this.http.get('/api/items', {
      params: new HttpParams()
        .set('page', pageNumber.toString())
        .set('size', pageSize.toString())
    }).pipe(
      map(res => res['content'])
    )
  }

  getPlaceData() {
    return this.http.get('/api/places');
  }

  addNewItem(placeId, newItem) {
    return this.http.post('/api/places/' + placeId + '/items', newItem);
  }

  deleteExistingItem(record: ItemRecord): Observable<{}> {
    console.log('Delete existing item called: ' + record.id);
    return this.http.delete('/api/items/' + record.id)
      // .pipe(
      //   catchError(e => of())
      // );
  }
}
