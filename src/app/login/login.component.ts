import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';
import { Validators, FormControl } from '@angular/forms';

interface User {
  username: string,
  password: string
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: User = {
    username: '',
    password: ''
  };

  usernameFormControl = new FormControl('', [
    Validators.required
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (this.loginService.isAuthenticated()) {
      this.router.navigate(['/data']);
    }
  }

  logUser() {
    if (this.usernameFormControl.valid && this.passwordFormControl.valid) {
      this.loginService.login(this.model).subscribe(isValid => {
        if (isValid) {
          console.log('Login response is valid');
          this.router.navigate(['/data']);
          return;
        } else {
          console.log('Login response is invalid');
        }
        // if (isValid) {
        //   sessionStorage.setItem(
        //     'token',
        //     btoa(this.model.username + ':' + this.model.password)
        //   );
        //   this.router.navigate(['/data']);
        // } else {
        //   alert('Authentication failed');
        // }
      });
    }
  }
}
