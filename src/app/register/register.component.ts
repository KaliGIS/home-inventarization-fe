import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../login.service';
import { UserService } from '../user.service';

interface User {
  username: string,
  email: string
  password: string,
  passwordVerif: string
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model: User = {
    username: '',
    email: '',
    password: '',
    passwordVerif: ''
  }
  
  usernameFormControl = new FormControl('', [
    Validators.required
  ]);
  emailFormControl = new FormControl('', [
    Validators.required
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required
  ]);
  passwordVerifFormControl = new FormControl('', [
    Validators.required,
    this.verifyPasswords(this.model.password, this.model.passwordVerif)
  ]);

  constructor(
    private router: Router,
    private userService: UserService,
    private loginService: LoginService
  ) {
    if (this.loginService.isAuthenticated()) {
      this.router.navigate(['/data']);
    }
  }

  ngOnInit(): void {
  }

  verifyPasswords(pass1: string, pass2: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any} | null => {
      if (pass1 === pass2) {
        return null;
      }
      return {passwordDoesNotMatch: control.value}
    } 
  }

  registerUser() {
    console.log('Register user called');
  }
}
