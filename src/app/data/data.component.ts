import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ItemService } from '../item.service';
import { ItemsDataSource } from '../items-data-source';
import { MatDialog } from '@angular/material/dialog';
import { NewItemComponent } from '../new-item/new-item.component';
import { DeletePromptComponent } from '../delete-prompt/delete-prompt.component';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css', '../app.component.css']
})
export class DataComponent implements OnInit {
  itemColumns: string[] = ['id', 'name', 'note', 'place', 'action'];
  itemData: ItemsDataSource;

  constructor(
    private itemService: ItemService,
    private loginService: LoginService,
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    console.log('data onInit');
    this.itemData = new ItemsDataSource(this.itemService, this.loginService, this.router);
    this.itemData.loadItems(0, 10);
  }

  openNewItemDialog(): void {
    this.itemService.getPlaceData().subscribe(places => {
      console.log(places);
      let emptyItem = {
        name: '',
        note: '',
        places: places
      }  
      
      const dialogRef = this.dialog.open(NewItemComponent, {
        width: '250px',
        data: emptyItem
      });
  
      dialogRef.afterClosed().subscribe(newItem => {
        console.log('The dialog was closed');
        console.log(newItem);
        this.itemData.loadItems(0, 10);
      });
    });
    
  }

  openShouldDeleteDialog(row): void {
    const dialogRef = this.dialog.open(DeletePromptComponent, {
      data: row
    });

    dialogRef.afterClosed().subscribe(shouldDelete => {
      console.log('Should delete: ' + shouldDelete);
      console.log('Type of shouldDelete: ' + typeof shouldDelete);
      
      if (shouldDelete) {
        this.itemService.deleteExistingItem(row).subscribe(
          res => console.log('HTTP response', res),
          err => console.error('HTTP Error', err),
          () => {
            console.log('HTTP request completed.');
            this.itemData.loadItems(0, 10);
          }
        );
      }
    });
  }
}