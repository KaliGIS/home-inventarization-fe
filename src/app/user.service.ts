import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  getUsers() {
    return this.http.get<any[]>('/api/users');
  }

  register(user) {
    return this.http.post('/api/users/register', user);
  }

  deleteUser(id) {
    return this.http.delete('/api/users/' + id);
  }
}
