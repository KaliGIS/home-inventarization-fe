import { Component, OnInit } from '@angular/core';
import { MaterialModule } from '../material/material.module';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {

  }

  isLoggedIn() {
    return this.loginService.isAuthenticated();
  }

  logout() {
    this.loginService.logout();
    this.router.navigate(['/login']);
  }
}
