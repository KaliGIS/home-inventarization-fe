import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface PlaceRecord {
  id: number,
  name: string;
  location: string;
  imageUrl: string;
}

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  constructor(
    private http: HttpClient
  ) { }

  getPlaceData() {
    return this.http.get<PlaceRecord[]>('/api/places');
  }

  addNewPlace(newPlace) {
    return this.http.post('/api/places', newPlace);
  }

  deleteExistingPlace(record: PlaceRecord): Observable<{}> {
    console.log('Delete existing item called: ' + record.id);
    return this.http.delete('/api/places/' + record.id)
  }
}
