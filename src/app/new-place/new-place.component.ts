import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PlaceService } from '../place.service';

interface NewPlaceData {
  id: number,
  location: string,
  name: string,
  imageUrl: string
}

@Component({
  selector: 'app-new-place',
  templateUrl: './new-place.component.html',
  styleUrls: ['./new-place.component.css']
})
export class NewPlaceComponent implements OnInit {
  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  locationFormControl = new FormControl('');

  constructor(
    public dialogRef: MatDialogRef<NewPlaceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: NewPlaceData,
    private placeService: PlaceService
  ) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(data) {
    console.log('Form submitted');
    if (this.nameFormControl.valid) {
      console.log('Name form control is valid');
      this.dialogRef.close();
      
      let placeToSend = {
        name: data.name,
        location: data.location,
        imageUrl: data.imageUrl
      }

      this.placeService.addNewPlace(placeToSend).subscribe(responseData => {
        console.log(responseData);
      });
    } else {
      console.log('Name form control is not valid');
    }
  }
}
